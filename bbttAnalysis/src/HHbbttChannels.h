#ifndef BBTTANALYSIS_CHANNELS
#define BBTTANALYSIS_CHANNELS

namespace HHBBTT
{

  enum Channel
  {
    LepHad = 0,
    HadHad = 1,
  };

}

#endif
